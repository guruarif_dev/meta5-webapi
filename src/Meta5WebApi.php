<?php 
namespace guruarif\Meta5;

use guruarif\Meta5\Entities\MTCommand;

define("PATH_TO_SCRIPTS", "Lib/");
include PATH_TO_SCRIPTS ."mt5_api.php";

/**
* Meta5WebApi
*/

class Meta5WebApi
{

	private $MT5_SERVER_IP = ""; //MT5 Server IP here

	private $MT5_SERVER_PORT = ""; //MT5 Server port here

	private $MT5_SERVER_WEB_LOGIN = ""; //MT5 Server login here

	private $MT5_SERVER_WEB_PASSWORD = ""; //MT5 Api password here

	public $api;

	private $response;

	function __construct($config = null)
	{
		
		$this->response['success'] = false;

		//If have custom config
		if ($config) {
			$this->MT5_SERVER_IP 			= $config['MT5_SERVER_IP'];
			$this->MT5_SERVER_PORT 			= $config['MT5_SERVER_PORT'];
			$this->MT5_SERVER_WEB_LOGIN 	= $config['MT5_SERVER_WEB_LOGIN'];
			$this->MT5_SERVER_WEB_PASSWORD 	= $config['MT5_SERVER_WEB_PASSWORD'];
		}

		if (empty($this->MT5_SERVER_IP) || empty($this->MT5_SERVER_PORT) || empty($this->MT5_SERVER_WEB_LOGIN) || empty($this->MT5_SERVER_WEB_PASSWORD)) {
			$this->response['message'] = 'Config information must not be empty!';
		}else{

			$this->api = new \MTWebAPI();

			if (!$this->api->IsConnected()){
		      	if (($error_code = $this->api->Connect($this->MT5_SERVER_IP, $this->MT5_SERVER_PORT, 300, $this->MT5_SERVER_WEB_LOGIN, $this->MT5_SERVER_WEB_PASSWORD)) != \MTRetCode::MT_RET_OK){
		      		$this->response['error']['Code'] = $error_code;
		        	$this->response['error']['Description'] = \MTRetCode::GetError($error_code);
		        }else{
		        	$this->response['message'] = 'Connect to server successfully!';
		        }
		    }else{
		        $this->response['message'] = 'Already connected with server!';
		    }
		}
	}

	public function execute($command = null, $data = null){
		if(MTCommand::hasCommand($command)){
			$error_code = $this->$command($data);
			if ($error_code != \MTRetCode::MT_RET_OK){
		    	$this->response['error']['Code'] =  $error_code;
		    	$this->response['error']['Description'] =  \MTRetCode::GetError($error_code);
		    }else{
		    	$this->response['message'] = 'Command successfully executed!';
		    }
		}else{
			$this->response['message'] = 'Command does not exits!';	
		}
		
		return $this->response;
	}

	public function Disconnect(){

		return $this->api->Disconnect();
	}

	public function IsConnected(){

		return $this->api->IsConnected();
	}

	public function AccountCreate($data){

		$new_user = $this->api->UserCreate();
		$new_user->Login = $data['Login'];
		$new_user->Email = $data['Email'];
		$new_user->MainPassword = $data['Password'];
		$new_user->Group = $data['Group'];
		$new_user->Leverage = $data['Leverage'];
		$new_user->ZipCode = $data['ZipCode'];
		$new_user->Country = $data['Country'];
		$new_user->State = $data['State'];
		$new_user->City = $data['City'];
		$new_user->Address = $data['Address'];
		$new_user->Phone = $data['Phone'];
		$new_user->Name = $data['Name'];
		$new_user->PhonePassword = $data['PhonePassword'];
		$new_user->InvestPassword = $data['InvestPassword'];
		
		//--- add us
	    $error_code = $this->api->UserAdd($new_user, $user_server);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$new_user->Login = $user_server->Login;
	    	$user_server->MainPassword = $data['Password'];
	    	$user_server->PhonePassword = $data['PhonePassword'];
	    	$user_server->InvestPassword = $data['InvestPassword'];
	    	$this->response['success'] = true;
	    	$this->response['data'] = (array) $user_server;
	    }

	    return $error_code;
	}

	public function AccountUpdate($data){

		$up_user = $this->api->UserCreate();
		foreach($data as $key=>$value) {  
		    $up_user->$key = $value;
		} 
		
		$error_code = $this->api->UserUpdate($up_user, $user_server);

		if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    	$this->response['message'] = 'Account successfully updated!';
	    	$this->response['data'] = (array) $user_server;
	    }

	    return $error_code;
	}

	public function BalanceUpdate($data){

		$login = $data['Login'];
		$balance = $data['Balance'];
		$comment = $data['Comment'];
		
		$error_code = $this->api->TradeBalance($login, \MTEnDealAction::DEAL_BALANCE, $balance, $comment, $ticket);

		if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    	$this->response['data']['order'] = $ticket;
	    }

	    return $error_code;
	}

	public function CreditUpdate($data){

		$login = $data['Login'];
		$balance = $data['Balance'];
		$comment = $data['Comment'];
		
		$error_code = $this->api->TradeBalance($login, \MTEnDealAction::DEAL_CREDIT, $balance, $comment, $ticket);

		if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    	$this->response['data']['order'] = $ticket;
	    }

	    return $error_code;
	}

	public function UserDataGet($data){
		$login = $data['Login'];
	    $error_code = $this->api->UserGet($login, $user_server);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    	$this->response['data'] = (array) $user_server;
	    }
	    return $error_code;
	}

	public function AccountCheckPassword($data){
		$login = $data['Login'];
		$password = $data['Password'];
	    $error_code = $this->api->UserPasswordCheck($login, $password);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    }
	    return $error_code;
	}

	public function AccountCheckInvestorPassword($data){
		$login = $data['Login'];
		$password = $data['Password'];
	    $error_code = $this->api->UserPasswordCheck($login, $password, \MTProtocolConsts::WEB_VAL_USER_PASS_INVESTOR);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    }
	    return $error_code;
	}

	public function AccountChangePassword($data){
		$login = $data['Login'];
		$password = $data['Password'];
	    $error_code = $this->api->UserPasswordChange($login, $password);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    }
	    return $error_code;
	}

	public function AccountChangeInvestorPassword($data){
		$login = $data['Login'];
		$password = $data['Password'];
	    $error_code = $this->api->UserPasswordChange($login, $password, \MTProtocolConsts::WEB_VAL_USER_PASS_INVESTOR);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    }
	    return $error_code;
	}

	public function AccountGetMargin($data){
		$login = $data['Login'];
	    $error_code = $this->api->UserAccountGet($login, $user_server);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    	$this->response['data'] = (array) $user_server;
	    }
	    return $error_code;
	}

	public function GroupGet($data){
		$position = $data['Position'];
	    $error_code = $this->api->GroupNext($position, $group);
	    if ($error_code == \MTRetCode::MT_RET_OK) {
	    	$this->response['success'] = true;
	    	$this->response['data'] = (array) $group;
	    }
	    return $error_code;
    }
    
    private function UsersGetTotal($data){
        //--- send custom command
        $params       = $data;
        $answer       = array();
        $answer_boddy = array();

        $error_code = $this->api->CustomSend('APIEXT_TOTAL_USERS', $params, '', $answer, $answer_boddy);
        if ($error_code == \MTRetCode::MT_RET_OK) {
            $this->response['success'] = true;
            $this->response['data'] = (array) $answer;
        }
        return $error_code;
    }

    public function test(){
        //--- send custom command
        $params       = array('GROUP' => 'WL_OXIS_B_01');
        $answer       = array();
        $answer_boddy = array();

        $error_code = $this->api->CustomSend('USER_LOGINS', $params, '', $answer, $answer_boddy);

        print_r($answer);
        print_r($answer_boddy);
        print_r($error_code);
    }

    public function sendSMS(){
        //--- send custom command
        $params       = array(
        	'TO' => '+8801762112077',
        	'FROM' => '+8801762112077',
        	'GROUP' => 'WL_OXIS_B_01',
        	'TEXT' => 'Send from guruarif!',
        );
        $answer       = array();
        $answer_boddy = array();

        $error_code = $this->api->CustomSend('MESSENGER_SEND', $params, '', $answer, $answer_boddy);

        print_r($answer);
        print_r($answer_boddy);
        print_r($error_code);
    }
}
